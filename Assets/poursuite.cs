using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poursuite : MonoBehaviour
{
    
    public float v; //vitesse déplacement du chat
    public GameObject Cible;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 C;
        Vector3 S;
        Vector3 SC;
        S = Cible.transform.position;
        C=this.transform.position;
        SC = (S-C).normalized;
        this.transform.position += SC * v * Time.deltaTime;
        this.transform.rotation = Quaternion.LookRotation(-SC);
    }
}
