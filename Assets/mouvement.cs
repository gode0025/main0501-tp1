using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouvement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.DownArrow)){
            transform.Translate(Vector3.forward * 0.01f);
        }
        if(Input.GetKey(KeyCode.UpArrow)){
            transform.Translate(Vector3.back * 0.01f);
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.Translate(Vector3.back * 0.05f);
            }
        }
        if(Input.GetKey(KeyCode.LeftArrow)){
            transform.Rotate(Vector3.up, -2);
        }
        if(Input.GetKey(KeyCode.RightArrow)){
            transform.Rotate(Vector3.up, 2);
        }
        Rigidbody rb = GetComponent<Rigidbody>();
        if(Input.GetKeyDown(KeyCode.Space) && transform.position.y <= 1.1){
            rb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        }
    }
}
